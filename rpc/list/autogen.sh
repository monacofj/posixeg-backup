#!/bin/sh
#
#  autogen.sh - Autootols boostrap for developers
#    
#    Copyright 2015  Francisco Jose Monaco   <monaco@icmc.usp.br>
#
#    This file is part of ProjectName. 
#
#    ProjectName is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Lesser General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    ProjectName is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Lesser General Public License
#    along with .  If not, see <http://www.gnu.org/licenses/>.
#

# We need autoreconf

AUTORECONF=$(which autoreconf)
if test -z "$AUTORECONF"; then
  echo "Program autoreconf not found"
  exit 1
fi

# Automake requires some files to be present

NEEDEDFILES="NEWS README AUTHORS ChangeLog"
for file in $NEEDEDFILES ; do
    if ! test -f $file ; then
	touch $file
    fi
done

# If we are using m4 subdir

if ! test -d m4; then
    mkdir m4
fi

echo -n "Bootstrapping autotools generated files..."

# Install missing files

$AUTORECONF --install

echo "Done"
