/*  mylist.h - Linked list
    
    Copyright 2015  (c) Authors

    This file is part of ProjectName. 

    ProjectName is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ProjectName is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with .  If not, see <http://www.gnu.org/licenses/>. 

*/


#ifndef ProjectName_MYLIST_H
#define ProjectName_MYLIST_H

/* Opaque data type representing the head of a linked list. */

typedef void list_head_t;

/* Opaque data type representing a node of a linked list. The node
   value stores an integer value. */

typedef void list_node_t;

/* Return a reference for a newly allocated list head. On failure,
   return NULL.*/

void * new_list_head (void);

/* Append a node to the list tail and assign it the supplied value. On
   success, return 0; on failure return non-zero.*/

int append_node (int value);

/* Sort the list in ascendant order. */

void sort_list (list_head_t *list);

/* Return a reference to the first node of the list, or NULL if
   the list is empty. */

list_node_t *get_first (list_head_t *list);

/* Return the integer value stored in the given list node.*/

int get_node_value (list_node_t *node);

/* Given a list node, return a reference to the next node
   in the list. If the node is the last node, return NULL. */

list_node_t *get_next (list_node_t *node);

/* Given a list node, remove-it from the list. If the node
   is not on the list, do nothing.*/

void remove_node (list_head_t * , list_node_t *);



#endif   /*ProjectName_MYLIST_H */
