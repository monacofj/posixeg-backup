/*  mylist.c - Linked list
    
    Copyright 2015  (c) Authors

    This file is part of ProjectName. 

    ProjectName is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ProjectName is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with .  If not, see <http://www.gnu.org/licenses/>. 

*/

#include <stdio.h>
#include <mylist.h>
#include <myrpc.h>

/* 
   Implementation of the API described in mylist.h
   RPC version.
*/

list_head_t * new_list_head (void)
{
  return NULL;
}

int append_node (int value)
{
  return 0;
}

void sort_list (list_head_t *list)
{
}

list_node_t *get_first (list_head_t *list)
{
  return NULL;
}

list_node_t *get_next (list_node_t *node)
{
  return NULL;
}

int get_node_value (list_node_t *node)
{
  return 0;
}

void remove_node (list_head_t * list, list_node_t *node)
{
}
